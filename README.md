# Readme
This package supports other packages that I write. I ended up using it so often that I separated it out.

# Installation
It can now be install via pip.

```
pip install traceutils2
```

or updated with

```
pip install -U traceutils2
```
